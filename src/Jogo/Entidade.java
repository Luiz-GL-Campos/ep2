package Jogo;

import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Entidade {
	private int x, y, tamanho;
	public Entidade(int tamanho){
		this.tamanho = tamanho;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY(){
		return y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void movimento(int dx, int dy) {
		x += dx;
		y += dy;
	}
	
	public Rectangle getBound() {
		return new Rectangle(x, y, tamanho, tamanho);
	}
	
	public boolean Colisao(Entidade o) {
		if(o == this) return false;
		return getBound().intersects(o.getBound());
	}
	
	public void render(Graphics2D g2d) {
		g2d.fillRect(x + 1, y + 1, tamanho - 2, tamanho - 2);
	}
}