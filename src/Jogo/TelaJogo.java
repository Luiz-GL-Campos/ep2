package Jogo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class TelaJogo extends JPanel implements Runnable, KeyListener {
	
	public static final int LARGURA = 400;
	public static final int ALTURA = 400;
	
	//Render
	private Graphics2D g2d;
	private BufferedImage imagem;
	
	//Game Loop
	private Thread thread;
	private boolean executando;
	private long targetTime;

	//Coisas/caracteristicas do Jogo
	private final int TAMANHO = 10;
	private Entidade cabeca, maca;
	private ArrayList<Entidade> cobra;
	private int pontos;
	private int level;
	private boolean gameover;
	
	//Movimento
	private int dx, dy;
	
	//Config teclas
	private boolean up, down, right, left, start;
	
	public TelaJogo(){
		setPreferredSize(new Dimension(LARGURA, ALTURA));
		setFocusable(true);
		requestFocus();
		addKeyListener(this);
	}
	
	@Override
	public void addNotify() {
		super.addNotify();
		thread = new Thread(this);
		thread.start();
	}
	private void setFPS(int fps) {
		targetTime = 1000 / fps;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		int k = e.getKeyCode();
		
		if(k == KeyEvent.VK_UP) up = true;
		if(k == KeyEvent.VK_DOWN) down = true;
		if(k == KeyEvent.VK_LEFT) left = true;
		if(k == KeyEvent.VK_RIGHT) right = true;
		if(k == KeyEvent.VK_ENTER) start = true;

	}

	@Override
	public void keyReleased(KeyEvent e) {
		int k = e.getKeyCode();
		
		if(k == KeyEvent.VK_UP) up = false;
		if(k == KeyEvent.VK_DOWN) down = false;
		if(k == KeyEvent.VK_LEFT) left = false;
		if(k == KeyEvent.VK_RIGHT) right = false;
		if(k == KeyEvent.VK_ENTER) start = false;


	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		

	}

	@Override
	public void run() {
		if(executando) return;
		init();
		long startTime;
		long elapsed;
		long espera;
		while(executando) {
			startTime = System.nanoTime();
			
			update();
			RequestRender();
			
			elapsed = System.nanoTime() - startTime;
			espera = targetTime - elapsed / 1000000;
			if (espera > 0) {
				try {
					Thread.sleep(espera);
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void init() {
		imagem = new BufferedImage(LARGURA, ALTURA, BufferedImage.TYPE_INT_ARGB);
		g2d = imagem.createGraphics();
		executando = true;
		setUplevel();
		
	}
	
	private void setUplevel() {
		cobra = new ArrayList<Entidade>();
		cabeca = new Entidade(TAMANHO);
		cabeca.setPosition(LARGURA / 2, ALTURA / 2);
		cobra.add(cabeca);
		
		for(int i = 1; i < 3; i++) { //Tamanho padrão da cobrinha
			Entidade e = new Entidade (TAMANHO);
			e.setPosition(cabeca.getX() + (i * TAMANHO), cabeca.getY());
			cobra.add(e);
			
		}
		maca = new Entidade(TAMANHO);
		setMaca();
		pontos = 0;
		gameover = false;
		level = 1;
		dx = dy = 0;
		setFPS(level * 10);
	}
	
	public void setMaca(){
		int x = (int)(Math.random() * (LARGURA - TAMANHO));
		int y = (int)(Math.random() * (ALTURA - TAMANHO));
		x = x - (x % TAMANHO);	//Define o lugar correto da maçã
		y = y - (y % TAMANHO);	//(nos mesmos limites da cobra)
		maca.setPosition(x,y);
	}
	
	private void RequestRender() {
		render(g2d);
		Graphics g = getGraphics();
		g.drawImage(imagem, 0, 0, null);
		g.dispose();
	}

	private void update() {
		if(gameover){
			if(start) {
				setUplevel();
			}
			return;
		}
		
		if(up && dy == 0) {
			dy = -TAMANHO;
			dx = 0;
		}
		if(down && dy == 0) {
			dy = TAMANHO;
			dx = 0;
		}
		if(left && dx == 0) {
			dy = 0;
			dx = -TAMANHO;
		}
		if(right && dx == 0 && dy !=0) {
			dy = 0;
			dx = TAMANHO;
		}
		if(dx != 0 || dy != 0){
			for (int i = cobra.size() - 1; i > 0; i--){
				cobra.get(i).setPosition(
						cobra.get(i - 1).getX(),
						cobra.get(i - 1).getY()
						);
			}
			cabeca.movimento(dx, dy);
		}
		
		for(Entidade e : cobra) {
			if(e.Colisao(cabeca)) {
				gameover = true;
				break;
			}
		}
		
		if(maca.Colisao(cabeca)) {
			pontos++;
			setMaca();
			
			Entidade e = new Entidade (TAMANHO); //Aumenta o tamanho conforme pega as maçãs 
			e.setPosition(-100, -100);
			cobra.add(e);
			
			if(pontos % 10 == 0) {
				level++;
				if(level > 10) level = 10;
				setFPS(level * 10); //aumenta em 10x a velocidade a cada 10 pontos
			}
		}
		
		if(cabeca.getX() < 0) cabeca.setX(LARGURA);
		if(cabeca.getY() < 0) cabeca.setY(ALTURA);
		if(cabeca.getX() > LARGURA) cabeca.setX(0);
		if(cabeca.getY() > ALTURA) cabeca.setY(0);
	}
	public void render (Graphics2D g2d) {
		g2d.clearRect(0, 0, LARGURA, ALTURA);
		g2d.setColor(Color.GREEN);
		for(Entidade e : cobra) {
			e.render(g2d);
		}
		g2d.setColor(Color.RED);
		maca.render(g2d);
		
		if(gameover) {
			g2d.drawString("GameOver!", 150, 200); //Mensagem quando perder o jogo
		}
		
		g2d.setColor(Color.WHITE);
		g2d.drawString("Pontos: " + pontos + " " + "Level: " + level, 10, 10);
		
		if(dx == 0 && dy == 0){
			g2d.drawString("Ready!", 150, 200); //Mensagem para inicio do jogo
		}
	}
	

}